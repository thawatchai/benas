import { Route, Switch, useLocation } from 'react-router-dom'
import React from 'react'
import { useTransition, animated } from 'react-spring'

import Home from './pages/Home/index.js'
import WhereToBuy from './pages/WhereToBuy'
import PageNotFound from './pages/PageNotFound'

const Routes = () => {
  const location = useLocation()
  const transitions = useTransition(location, (location) => location.pathname, {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
    config: { duration: 100 }
  })

  return transitions.map(({ item: location, props, key }) => (
    <animated.div key={key} style={props}>
      <Switch location={location}>
        <Route exact path='/' component={Home} />
        <Route exact path='/where-to-buy' component={WhereToBuy} />\
        <Route component={PageNotFound} />
      </Switch>
    </animated.div>
  ))
}

export default Routes
