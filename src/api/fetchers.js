import createFetcher from './createFetcher'

const host = process.env.REACT_APP_HOST || ''
const useMock = true

const fetchContent = (params) =>
  createFetcher({
    useMock,
    method: 'get',
    url: `${host}/wp-json/acf/v3/options/options`,
    params,
    jsonMock: 'content.json',
    delay: 0
  })

const fetchWhereToBuy = (params) =>
  createFetcher({
    useMock,
    method: 'get',
    url: `${host}/wp-json/wp/v2/where_to_buy`,
    params,
    jsonMock: 'whereToBuy.json',
    delay: 0
  })

export { fetchContent, fetchWhereToBuy }
