import { createActionWithFetching } from '../utils'
import { fetchContent, fetchWhereToBuy } from '../api/fetchers'
import dataModule from '../modules/data'

const getContent = () => {
  const callAction = async (dispatch) => {
    const [content, whereToBuy] = await Promise.all([fetchContent(), fetchWhereToBuy()])

    dispatch(
      dataModule.set({
        key: 'content',
        value: content.data.acf
      })
    )

    dispatch(
      dataModule.set({
        key: 'whereToBuy',
        value: whereToBuy.data
      })
    )
  }

  return createActionWithFetching({
    callAction
  })
}

export { getContent }
