import styled from '@emotion/styled'
import { Helmet } from 'react-helmet'
import { useSelector } from 'react-redux'
import isEmpty from 'lodash/isEmpty'

const AboutUs = () => {
  // const appData = useSelector((state) => state.data)

  // if (isEmpty(appData.content)) return null

  // const { content } = appData

  return (
    <>
      <Helmet>
        {/* <title>{content.meta_title_about}</title>
        <meta name='title' content={content.meta_title_about} />
        <meta name='description' content={content.meta_description_about} />

        <meta property='og:type' content='website' />
        <meta property='og:url' content='https://www.onmywatch-shop.com/about-us' />
        <meta property='og:title' content={content.meta_title_about} />
        <meta property='og:description' content={content.meta_description_about} />
        <meta property='og:image' content='https://www.onmywatch-shop.com/on-my-watch.png' />
        <meta property='og:image:type' content='image/*' />

        <meta property='twitter:card' content='summary_large_image' />
        <meta property='twitter:url' content='https://www.onmywatch-shop.com/about-us' />
        <meta property='twitter:title' content={content.meta_title_about} />
        <meta property='twitter:description' content={content.meta_description_about} />
        <meta
          property='twitter:image'
          content='https://www.onmywatch-shop.com/on-my-watch.png'
        ></meta> */}
      </Helmet>

      <Style>About Us</Style>
    </>
  )
}

const Style = styled('div')`
  label: AboutUs;
`

export default AboutUs
