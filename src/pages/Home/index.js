import { useEffect } from 'react'
import styled from '@emotion/styled'
import { useDispatch, useSelector } from 'react-redux'
import isEmpty from 'lodash/isEmpty'
import { Helmet } from 'react-helmet'

import Welcome from './Welcome'
import Introduction from './Introduction'
import WhereToBuy from './WhereToBuy'

const Home = () => {
  const dispatch = useDispatch()
  const appData = useSelector((state) => state.data)

  useEffect(() => {
    // dispatch(getContent())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (isEmpty(appData.content)) return <Style />
  const { content, whereToBuy } = appData

  return (
    <Style>
      <Helmet>
        {/* <title>{content.meta_title_home}</title>
        <meta name='title' content={content.meta_title_home} />
        <meta name='description' content={content.meta_description_home} />

        <meta property='og:type' content='website' />
        <meta property='og:url' content='https://www.onmywatch-shop.com/' />
        <meta property='og:title' content={content.meta_title_home} />
        <meta property='og:description' content={content.meta_description_home} />
        <meta property='og:image' content='https://www.onmywatch-shop.com/on-my-watch.png' />
        <meta property='og:image:type' content='image/*' />

        <meta property='twitter:card' content='summary_large_image' />
        <meta property='twitter:url' content='https://www.onmywatch-shop.com/' />
        <meta property='twitter:title' content={content.meta_title_home} />
        <meta property='twitter:description' content={content.meta_description_home} />
        <meta
          property='twitter:image'
          content='https://www.onmywatch-shop.com/on-my-watch.png'
        ></meta> */}
      </Helmet>
      <Welcome />
      <Introduction />
      <WhereToBuy items={whereToBuy} />
    </Style>
  )
}

const Style = styled('div')`
  min-height: 100vh;
`

export default Home
