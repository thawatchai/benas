import styled from '@emotion/styled'
import Slider from 'react-slick'
import { useRef } from 'react'

import { breakpoint, fluidSizing } from '../../utils'
import { paddingFooter } from '../../components/Footer'

const settings = {
  dots: false,
  arrows: false,
  fade: false,
  autoplay: false,
  infinite: true,
  speed: 500,
  autoplaySpeed: 4000,
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    // {
    //   breakpoint: 768,
    //   settings: {
    //     slidesToShow: 3,
    //     slidesToScroll: 3,
    //     dots: true
    //   }
    // }
  ]
}

const WhereToBuy = ({ items }) => {
  const sliderRef = useRef()

  return (
    <Style>
      <div className='container'>
        <div className='row'>
          <div className='D-3 M-12 vertical-middle'>
            <h1>Where to buy</h1>
          </div>
          <div className='D-9 M-12 vertical-middle'>
            <div className='box-slider'>
              <img
                className='prev'
                onClick={() => sliderRef.current.slickPrev()}
                src='/images/ico-arrow-slider-left.webp'
                alt=''
              />
              <img
                className='next'
                onClick={() => sliderRef.current.slickNext()}
                src='/images/ico-arrow-slider-right.webp'
                alt=''
              />
              <Slider ref={sliderRef} {...settings}>
                {items.map((item, i) => (
                  <div key={i} className='slider-item'>
                    <img key={i} src={item.image} alt='' />
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </div>
      </div>
    </Style>
  )
}

const Style = styled.div`
  label: Introduction;

  position: relative;
  min-height: 100vh;
  background: #fff url('/images/img-where-to-buy.webp') center center no-repeat;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 160px 20px;

  h1 {
    font-size: 44px;
    width: 150px;
    text-transform: uppercase;
  }

  .box-slider {
    position: relative;
    margin: 0 60px;

    .prev {
      position: absolute;
      top: 50%;
      left: -40px;
      transform: translateY(-50%);
      width: 27px;
      cursor: pointer;
    }

    .next {
      position: absolute;
      top: 50%;
      right: -40px;
      transform: translateY(-50%);
      width: 27px;
      cursor: pointer;
    }
  }

  .slider-item {
    padding: 20px;
    img {
      width: 100%;
    }
  }

  ${breakpoint('M')} {
    background: #fff url('/images/img-where-to-buy-mobile.webp') center center no-repeat;
    background-size: cover;
    padding-top: 60px;
    min-height: auto;

    .box-slider {
      margin: 0;
    }

    .prev,
    .next {
      display: none;
    }

    .slider-item {
      padding: 10px;
    }

    h1 {
      font-size: 20px;
      width: 75px;
      line-height: 1.2;
    }
  }

  ${paddingFooter}
`

export default WhereToBuy
