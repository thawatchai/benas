import PropTypes from 'prop-types'
import React from 'react'
import styled from '@emotion/styled'

const Button = (props) => {
  return <Style className='btn'>{props.children}</Style>
}

Button.propTypes = {
  onClick: PropTypes.func
}

Button.defaultProps = {
  onClick: () => null
}

export default Button

const Style = styled.div`
  label: Button;

  color: #fff;
  background: #4b0585;
  border-radius: 10.3px;
  padding: 15px 20px;
  font-size: 16px;
  display: inline-block;
  cursor: pointer;
`
