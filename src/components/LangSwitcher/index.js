import { useTranslation } from 'react-i18next'

const LangSwitcher = (props) => {
  const { i18n } = useTranslation()

  return (
    <>
      <button
        onClick={() => {
          i18n.changeLanguage('en')
        }}
      >
        EN
      </button>
      <button
        onClick={() => {
          i18n.changeLanguage('th')
        }}
      >
        TH
      </button>
    </>
  )
}

export default LangSwitcher
