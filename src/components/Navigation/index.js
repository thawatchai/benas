import { useState, useRef, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import { useChain, useTrail, useTransition, animated } from 'react-spring'
import styled from '@emotion/styled'

// import logo from '../../images/logo.svg'
import { history, breakpoint, fluidSizing } from '../../utils'
import Translate from '../Translate'
import LangSwitcher from '../LangSwitcher'

const menus = [
  {
    to: '/',
    label: 'Home'
  },
  {
    to: '/where-to-buy',
    label: 'Where to buy'
  }
]

const Navigation = (props) => {
  const [showNav, setShowNav] = useState(false)

  useEffect(() => {
    if (showNav) {
      document.body.style.position = 'fixed'
      document.body.style.top = `-${window.scrollY}px`
    } else {
      const scrollY = document.body.style.top
      document.body.style.position = ''
      document.body.style.top = ''
      window.scrollTo(0, parseInt(scrollY || '0') * -1)
    }
  }, [showNav])

  const menuRef = useRef()
  const menuItemRef = useRef()

  const trail = useTrail(menus.length, {
    ref: menuItemRef,
    config: { mass: 5, tension: 2000, friction: 200 },
    opacity: showNav ? 1 : 0
  })

  const menuTransition = useTransition(showNav, null, {
    ref: menuRef,
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 }
  })

  useChain(showNav ? [menuRef, menuItemRef] : [menuItemRef, menuRef], [0, 0.1])

  const handleToggleMenu = () => {
    setShowNav(!showNav)
  }
  return (
    <Style>
      <div className='logo' onClick={() => setShowNav(false)}>
        <img className='bg-logo' src='/images/bg-logo.png' alt='' />
        <img
          className='img-logo'
          src='/images/logo-benas.svg'
          alt=''
          onClick={() => history.push('/')}
        />
      </div>

      <div className='nav'>
        {menus.map((item, i) => (
          <div className='nav-item' key={i}>
            <NavLink exact to={item.to}>
              {item.label}
            </NavLink>
          </div>
        ))}
      </div>

      {menuTransition.map(
        (menu) =>
          menu.item && (
            <animated.div className='nav-mobile' style={menu.props}>
              <img
                src='/images/ico-close-modal.webp'
                alt=''
                className='btn-close'
                onClick={() => setShowNav(false)}
              />
              {trail.map(({ x, height, ...rest }, index) => {
                const item = menus[index]
                return (
                  <animated.div
                    key={index}
                    className='nav-item'
                    style={rest}
                    onClick={handleToggleMenu}
                  >
                    <NavLink exact to={item.to}>
                      {item.label}
                    </NavLink>
                  </animated.div>
                )
              })}
            </animated.div>
          )
      )}

      <div className={`nav-mobile-toggle ${showNav && 'open'}`} onClick={handleToggleMenu}>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </Style>
  )
}

export default Navigation

const Style = styled.div`
  label: Navigation;

  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  z-index: 2;
  display: flex;

  &:after {
    content: '';
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 190px;
    opacity: 0.5;
    z-index: 0;
    background: linear-gradient(
      180deg,
      rgba(0, 0, 0, 0.7) 0%,
      rgba(0, 0, 0, 0.28363095238095233) 50%,
      rgba(255, 255, 255, 0) 100%
    );
  }

  > .logo {
    position: relative;
    z-index: 1;
    flex-shrink: 0;

    > .bg-logo {
      width: 430px;
      margin-left: -40px;
    }
    > .img-logo {
      position: absolute;
      width: 128.2px;
      height: 39.7px;
      top: 24px;
      left: 57px;
      display: block;
      cursor: pointer;
    }
  }

  > .nav-mobile {
    display: none;
  }

  > .nav {
    position: relative;
    z-index: 1;
    margin-top: 25px;
    margin-right: 57px;
  }

  > .nav,
  .nav-mobile {
    margin-left: auto;

    > .nav-item {
      display: inline-block;

      a {
        color: #fff;
        display: block;
        padding: 2px 0;
        letter-spacing: 1.6px;
        transition: all 0.3s ease-in-out;
        text-transform: uppercase;
        text-decoration: none;
        border-bottom: 2px solid transparent;

        &.active {
          color: #993486;
          border-bottom: 2px solid #993486;
        }

        ${fluidSizing('font-size', { SM: 12, T: 12, D: 12 })}
        ${fluidSizing('margin-left', { SM: 10, T: 10, D: 20 })}
        ${fluidSizing('margin-right', { SM: 10, T: 10, D: 20 })}
      }
    }
  }

  > .nav-mobile-toggle {
    display: none;
    width: 30px;
    height: 20px;
    position: absolute;
    z-index: 1;
    top: 15px;
    right: 11px;
    cursor: pointer;

    > span {
      display: block;
      position: absolute;
      height: 2px;
      width: 100%;
      background: #fff;
      border-radius: 9px;
      opacity: 1;
      left: 0;
      transform: rotate(0deg);
      transition: all 0.25s ease-in-out;
    }

    > span:nth-child(1) {
      top: 0px;
      transform-origin: left center;
    }

    > span:nth-child(2) {
      top: 50%;
      margin-top: -1px;
      transform-origin: left center;
    }

    > span:nth-child(3) {
      bottom: 0;
      transform-origin: left center;
    }
  }

  ${breakpoint('1000px')} {
    > .nav {
      display: none;
    }

    > .nav-mobile {
      display: flex;
      position: fixed;
      z-index: 3;
      background: rgb(0 0 0 / 80%);
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      flex-direction: column;
      align-items: center;
      justify-content: center;

      .btn-close {
        position: absolute;
        top: 14px;
        right: 14px;
        width: 23px;
        cursor: pointer;
      }

      > .nav-item {
        margin: 15px 0;
      }
    }

    > .nav-mobile-toggle {
      display: inline-block;
    }
  }

  ${breakpoint('M')} {
    > .logo {
      margin-left: 14px;

      .bg-logo {
        width: 130px;
        margin: 0;
      }
      .img-logo {
        width: 62.9px;
        height: 19.5px;
        top: 10px;
        left: 22px;
      }
    }
  }
`
