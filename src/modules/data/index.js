import { createModule } from 'redux-modux'

const initialState = {
  content: {},
  whereToBuy: []
}

const handlers = {}

export default createModule({ moduleName: 'data', initialState, handlers })
